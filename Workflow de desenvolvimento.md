# Workflow de desenvolvimento

## Feature Branch Workflow

Para cada nova funcionalidade a ser desenvolvida, um novo branch é criado. Isso garante que o main nunca terá um código quebrado, facilitando ambientes de integração contínua. Ao terminar a implementação de cada nova funcionalidade, o branch é mesclado no principal, e então, deletado. 



![Feature Branch](https://static.imasters.com.br/wp-content/uploads/2013/10/git-workflow-feature-branch-1-300x262.png) 

### Como funciona

Comece com o branch master:

`git checkout master` - Entra no branch master

`git fetch origin` - Baixa as modificações do servidor

`git reset --hard origin/master` - Altera os arquivos locais para ficarem iguais ao origin





Crie um branch novo para cada feature nova a ser implementada

`git checkout -b new-feature master ` - Cria um novo branch se não existir (-b)  baseado no master



Edite, atualize, modifique, adicione os arquivos criando quantos commits forem necessários (localmente).

`git status   `

`git add <some-file> `

`git commit -m "Description" `



Dê um push no branch remoto criado anteriormente

`git push -u origin new-feature `



O trabalho em desenvolvimento terminou, agora o branch precisa ser aprovado e mesclado ao master:

`git checkout master ` - Entra no master

`git pull`  - Baixa as modificações do origin/master

`git pull origin new-feature`  - Baixa as modificações no origin/new-feature

Nesta etapa todo os conflitos precisam ser resolvidos e o código estar funcionando corretamente

`git push` - Aplica as modificações locais no origin/master



## Gitflow workflow

O Gitflow Workflow é utilizado na maioria das vezes em projetos grandes, onde é necessário ter um ciclo de desenvolvimento mais isolado; para isso, esse workflow utiliza branches para releases, features e bugfixes.

Nesse workflow, os branches master e develop são considerados históricos – isso porque eles irão guardar a história do projeto. As tags de marcação de release são feitas no master e o develop serve como branch de integração para branches de features.

Os branches de features são utilizados exatamente como no workflow anterior, a diferença aqui é que eles não serão mesclados no master e sim no develop.

Outra particularidade desse workflow é o branch de releases. Ele pode ser criado quando o branch de desenvolvimento (develop) estiver com features suficientes para uma release. Nesse branch nenhuma nova feature deve ser implementada, somente correções de bugs, inclusão de documentação ou tarefas relacionadas ao release.

Por último, há também o branch de hotfixes que é um branch de manutenção, ou seja, ele é feito a partir do master para corrigir alguma falha crítica e ao finalizar a correção, isso deve ser mesclado tanto no master quanto no develop.



![img](https://static.imasters.com.br/wp-content/uploads/2013/10/git-workflow-release-cycle-4maintenance.png) 



### Comandos usando o git-flow

![git-flow commands](https://danielkummer.github.io/git-flow-cheatsheet/img/git-flow-commands.png) 



